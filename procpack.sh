#! /bin/bash

pid="$1"
data_dir="$2"
tmp_dir="/tmp/dmp_procpack_${pid}-$$"

if [ "$pid" = "" ]; then
    echo "Usage: <pid> [data folder]"
    exit 1
fi

sudo rm -rf "$tmp_dir"
mkdir -p "$tmp_dir"
pushd "$tmp_dir"
if [ "$data_dir" = "" ]; then
    mkdir ./files
else
    cp -r "$data_dir" ./files || exit $?
fi

cp "$(which procbak)" ./procbak
sudo procbak dump "$pid" ./dump || exit $?
sudo chown -R "$(whoami)" ./dump
7z a dump.7z ./dump || exit $?
sudo rm -rf ./dump

if [ "$data_dir" = "" ]; then
    data_dir="$tmp_dir"
fi

printf '#! /bin/sh

ORIG_PID=%s
ORIG_LOC="%s"
THIS_LOC="$(pwd)"

rm -rf "$ORIG_LOC" || exit $?
cp -r ./files "$ORIG_LOC" || exit $?
cd "$ORIG_LOC"
rm -rf dump
7z x "${THIS_LOC}/dump.7z" || exit $?
echo "Process should be back alive shortly"
sudo "${THIS_LOC}/procbak" restore "$ORIG_PID" ./dump
rm -rf dump
' "$pid" "$data_dir" > deploy.sh
chmod a+x ./deploy.sh

popd
7z a ./"$pid".7z "$tmp_dir" && rm -rf "$tmp_dir"
