#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <criu/criu.h>


void print_usage(const char *prgn) {
    fprintf(stderr, "Usage: %s <dump/restore> <pid> [image directory]\n", prgn);
}

int get_exit_code() {
    return errno?errno:-1;
}

int main(int argc, char **argv) {
    // Get args
    if (argc < 3) {
        print_usage(argv[0]);
        return -1;
    }
    // Argument 1
    _Bool dump = strcmp(argv[1], "dump") == 0;
    if (!dump) {
        if (strcmp(argv[1], "restore") != 0) {
            print_usage(argv[0]);
            return -1;
        }
    }
    // Argument 2
    pid_t target_pid = atoi(argv[2]);
    // Argument 3
    char *image_dir = "/tmp/proc_baks";
    if (argc > 3) {
        image_dir = argv[3];
    }
    // Initialize CRIU
    if (criu_init_opts() < 0) {
        return get_exit_code();
    }
    // Create/Open target dir
    mkdir(image_dir, 77777);
    int image_dir_fd = open(image_dir, O_DIRECTORY);
    if (image_dir_fd < 0) {
        perror("Failed to open image directory");
        return get_exit_code();
    }
    // Set options
    criu_set_service_binary("criu");
    criu_set_pid(target_pid);
    criu_set_images_dir_fd(image_dir_fd);
    criu_set_shell_job(true);
    criu_set_file_locks(true);
    criu_set_tcp_close(true);
    // Perform operation
    {
        int res;
        if (dump) {
            res = criu_dump();
        } else {
            res = criu_restore();
        }
        if (res < 0 || (!dump && res == 0)) {
            if (errno) {
                perror("Failed to perform operation");
            } else {
                fprintf(stderr, "Unknown failure when performing operation\n");
            }
            return get_exit_code();
        }
    }
    // Wait for children to exit if in restore mode
    if (!dump) {
        while (1) {
            usleep(1000000);
            if (kill(target_pid, 0) == -1 && errno == ESRCH) {
                break;
            }
        }
    }
}
